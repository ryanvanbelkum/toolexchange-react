var express = require('express');
var app = express();

// Serve up content from public directory
// app.use(express.static(__dirname + '/'));
app.use(express.static(__dirname + '/'));
app.get('*', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});


// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); // get an instance of the express Router

app.listen(process.env.PORT || 80);
console.log("the server has started!");
