## To install:

`npm install`

## To run:

`gulp`

## The Guts

NOTE: Current React components may include business logic.  This isn't recommended and should be moved out of components.  Business logic should be housed in a pattern such as Redux (Flux), and will be refactored... eventually.