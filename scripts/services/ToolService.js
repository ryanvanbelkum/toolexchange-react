'use strict';

import h from '../helpers';

// Firebase
import Rebase  from 're-base';
let base = Rebase.createClass(h.rebase);


let ToolService = {

  addToolsToIndex: function(id, name, currentUser) {
    return base.push('toolIndex', {
      data: {garage: currentUser.username, toolId: id, name: name},
      then(err){
      }
    });
  },

  removeToolsToIndex: function(index) {
    const url = 'toolIndex/' + index;
    base.post(url, {
      data: null,
      then(err){
        if(!err){

        }
      }
    });
  },

  fetchTool: function(url, then) {
    base.fetch(url, {
      context: this,
      asArray: false,
      then: then
    });
  },

  returnTool: function(toolItem) {
    const borrowerUrl = `garages/${toolItem.borrower}/borrowed/${toolItem.borrowerKey}`,
          url = `garages/${toolItem.owner}/tools/${toolItem.toolKey}`;
    toolItem.date = null;
    toolItem.borrower = null;
    toolItem.borrowerKey = null;
    toolItem.url = null;

    base.post(url, {
      data: toolItem,
      then(err){
        if(!err){

        }
      }
    });
    base.post(borrowerUrl, {
      data: null,
      then(err){
        if(!err){

        }
      }
    });
    return toolItem;
  }

};

export default ToolService;
