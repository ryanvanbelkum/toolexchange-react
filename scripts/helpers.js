let helpers =  {
  rebase: {
    apiKey: 'AIzaSyCIB-r-zmdrYAb19lLqbosIbnmj0zyAT6o',
    authDomain: 'friendly-garage.firebaseapp.com',
    databaseURL: 'https://friendly-garage.firebaseio.com',
    storageBucket: 'friendly-garage.appspot.com',
    messagingSenderId: '534086287289'
  },
  getUserFromStorage: function(){
    return localStorage.getItem('user');
  },
  getUserName: function(email) {
    return email.substring( 0, email.indexOf( '@' ) );
  },
  checkIfHome: function(garageId) {
    let currentUser = JSON.parse(this.getUserFromStorage()),
        amIHome;
    if (currentUser) {
      amIHome = currentUser.username == garageId;
    } else {
      amIHome = false
    }
    return amIHome;
  },
  rebaseSanitize: function(string) {
    //todo: need to escape []
    return string.replace(/./g, '').replace(/#/g, '').replace(/$/g, '');
  }
};

export default helpers;


// firebase.database().ref("toolIndex").orderByChild("name").equalTo(this.props.params.searchTerm)
//     .once("value", snapshot => {
//       this.state.tools = snapshot.val();
//       this.setState({tools: this.state.tools});
//       console.log(this.state.tools);
// });
