import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';

import NotFound from './components/NotFound';
import GarageHome from './components/GarageHome';
import App from './components/App';
import Search from './components/Search';
import UserProfile from './components/UserProfile';

/*
  Routes
*/

const routes =
<div>
    <Router history={browserHistory}>
      <Route path="/" component={GarageHome} />
      <Route path="/garage/:garageId" component={App} />
      <Route path="/search/:searchTerm" component={Search} />
      <Route path="/profile/:user" component={UserProfile} />
      <Route path="*" component={NotFound} />
    </Router>
</div>;

ReactDOM.render(routes, document.querySelector('#main'));
