import React from 'react';
import autobind from 'autobind-decorator';
import {ButtonToolbar, Button, Popover, Tooltip, Modal, OverlayTrigger} from 'react-bootstrap';
import h from '../helpers';
import classNames from 'classnames';
import _ from 'lodash';
import { browserHistory } from 'react-router';

// firebase
import Rebase  from 're-base';
var base = Rebase.createClass(h.rebase);

@autobind
class GarageHome extends React.Component {

  constructor() {
    super();

    this.state = {
      user : {},
      availableUsers: [],
      showModal: false,
      errorMessage: '',
      formValidation: {
        username: true,
        firstName: true,
        lastName: true,
        email: true,
        telephone: true,
        password: true,
        verifyPassword: true,
        address: true
      }
    }
  }

  componentDidMount() {
    base.bindToState('userIndex', {
      context : this,
      state : 'availableUsers',
      asArray: false
    });
  }

  getUserByUid(uid) {
    base.fetch(`userIndex/${uid}`, {
      context: this,
      asArray: false,
      then(data){
        this.getStoredUser(data);
      }
    });
  }

  getStoredUser(username) {
    base.fetch(`garages/${username}/profile`, {
      context: this,
      asArray: false,
      then(data){
        this.setCurrentUser(data);
      }
    });
  }

  setCurrentUser(user) {
    localStorage.setItem('user',JSON.stringify(user));
    browserHistory.push(`/garage/${user.username}`);
  }

  isUserAvailable(username) {
    const users = _.values(this.state.availableUsers);
    return _.indexOf(users, username) === -1
  }

  createNewUser() {
    let email = this.refs.email.value,
        password = this.refs.password.value;
    if (this.isUserAvailable(this.refs.username.value)){
    base.auth().createUserWithEmailAndPassword(email, password).then(user => {
      let currentUser = {
        firstName: this.refs.firstName.value,
        lastName: this.refs.lastName.value,
        username: this.refs.username.value,
        telephone: this.refs.telephone.value,
        email: this.refs.email.value,
        address: this.refs.address ? this.refs.address.value : ''
      };

      base.post(`garages/${currentUser.username}/profile`, {
        data: currentUser
      }).then(() => {
        this.close();
        this.setUserByUid(user.uid, currentUser.username);
        this.setCurrentUser(currentUser);
      }).catch(err => {
        // handle error
      });
    },
    error => {
      this.setErrorMessage(error.message);
    });
  } else {
    this.setErrorMessage('Username already exists');
  }
}

  signinUser() {
    let email = this.refs.userEmail.value,
        password = this.refs.userpass.value;

    base.authWithPassword({
      email    : email,
      password : password
    }, (error, user) => {
      if(error) this.setState({errorMessage: error.message});
      if (!error) this.getUserByUid(user.uid);
    });
  }

  setUserByUid(uid, username) {
    base.post(`userIndex/${uid}`, {
      data: username
    });
  }

  setErrorMessage(msg) {
    this.setState({ errorMessage : msg });
  }

  close() {
  this.setState({ showModal: false, errorMessage: '' });
  }

  open() {
    this.setState({ showModal: true, errorMessage: '' });
  }

  validate(callback) {
    const username = this.refs.username.value;
    let userVal = true, errorMessage = null;
    if (username.includes('.') || username.includes('#') || username.includes('$') ||
          username.includes('[') || username.includes(']')) userVal = false
    let formValidation = {
      firstName: this.refs.firstName.value ? true : false,
      lastName: this.refs.lastName.value ? true : false,
      telephone: this.refs.telephone.value ? true : false,
      email: this.refs.email.value ? true : false,
      address: this.refs.address.value ? true : false,
      password: this.refs.password.value && this.refs.verifyPassword.value ? true : false,
      username: userVal
    };
    if (formValidation.password) {
      formValidation.password = this.refs.password.value === this.refs.verifyPassword.value;
      if (!formValidation.password) errorMessage = 'passwords do not match';
    }
    this.setState({formValidation : formValidation, errorMessage: errorMessage});
    if (formValidation.firstName && formValidation.lastName && formValidation.telephone &&
        formValidation.email && formValidation.address && formValidation.password &&
          formValidation.username) callback()
  }

  render() {
    const popover = (
      <Popover id="modal-popover" title="popover">
        very popover. such engagement
      </Popover>
    ),
    tooltip = (
      <Tooltip id="modal-tooltip">
        wow.
      </Tooltip>
    ),
    firstNameVal = classNames('form-group form-group-home-modal',{
      'has-error': !this.state.formValidation.firstName
    }),
    lastNameVal = classNames('form-group form-group-home-modal',{
      'has-error': !this.state.formValidation.lastName
    }),
    emailVal = classNames('form-group form-group-home-modal',{
      'has-error': !this.state.formValidation.email
    }),
    telVal = classNames('form-group form-group-home-modal',{
      'has-error': !this.state.formValidation.telephone
    }),
    passVal = classNames('form-group form-group-home-modal',{
      'has-error': !this.state.formValidation.password
    }),
    usernameVal = classNames('form-group form-group-home-modal',{
      'has-error': !this.state.formValidation.username
    }),
    addressVal = classNames('form-group form-group-home-modal',{
      'has-error': !this.state.formValidation.address
    });


    return (
      <div className="tool-content tool-content-size">
        <div className='tool-content-header'><strong>Tool</strong> Co-op</div>
        <form className='signin-form'>
          <div className="signin-sub-header">Need a tool? Borrow from your friends! </div>
          <div className="form-group form-group-garage-home">
          <input ref="userEmail" type="text" className="form-control user-input" placeholder="Email" />
          <input ref="userpass" type="password" className="form-control user-input" placeholder="Password" />
          </div>
          <button onClick={this.signinUser} type="reset" className="btn btn-primary form-group-garage-home">Sign In</button>
          <button onClick={this.open} type="reset" className="btn btn-success form-group-garage-home">Sign Up</button>
        </form>
        <hr />
        <a className="signin-email-help" href="mailto:ryanvanbelkum@gmail.com?Subject=Tool%20Coop" target="_top">Need help?</a>
        <div className="garage-home-errorMessage">{this.state.errorMessage}</div>
        <div>

        <Modal show={this.state.showModal} onHide={this.close}>
          <Modal.Header closeButton>
            <Modal.Title>Sign Up Your Garage</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <form>
            <div className={usernameVal}>
              <label htmlFor="exampleInputEmail1">Username (Cannot contain, . # $ [ ])</label>
              <input ref="username" type="text" className="form-control" placeholder="Enter username" />
            </div>
            <div className={firstNameVal}>
              <label htmlFor="exampleInputEmail1">First Name</label>
              <input ref="firstName" type="text" className="form-control" placeholder="Enter firstname" />
            </div>
            <div className={lastNameVal}>
              <label htmlFor="exampleInputEmail1">Last Name</label>
              <input ref="lastName" type="text" className="form-control" placeholder="Enter lastname" />
            </div>
            <div className={telVal}>
              <label htmlFor="example-tel-input">Telephone</label>
              <input ref="telephone" className="form-control" type="tel" placeholder="1-(555)-555-5555" />
            </div>
            <div className={emailVal}>
              <label htmlFor="exampleInputEmail1">Email address</label>
              <input ref="email" type="email" className="form-control" placeholder="Enter email" />
            </div>
            <div className={passVal}>
              <label htmlFor="exampleInputPassword1">Password</label>
              <input ref="password" type="password" className="form-control" placeholder="Password" />
            </div>
            <div className={passVal}>
              <label htmlFor="exampleInputPassword2">Password</label>
              <input ref="verifyPassword" type="password" className="form-control" placeholder="Verify Password" />
            </div>
            <div className={addressVal}>
              <label htmlFor="exampleTextarea">Address</label>
              <textarea ref="address" className="form-control" rows="2"></textarea>
            </div>
            <button type="button" onClick={() => this.validate(this.createNewUser)} className="btn btn-primary">Submit</button>
            </form>
          </Modal.Body>
          <div className="garage-home-errorMessage">{this.state.errorMessage}</div>
          <Modal.Footer>
            <Button onClick={this.close}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
      </div>
    )
  }
}


export default GarageHome;
