import React from 'react';
import moment from 'moment';
import classNames from 'classnames';
import h from '../helpers';

const Tool = ({tool, username, borrowedTool, toolKey, removeTool, borrowTool, returnTool}) => {
  const borrowedDate = tool.date ? moment(tool.date).format('MMMM Do YYYY') : "Available",
    amIHome = h.checkIfHome(username),
    rowClass = classNames('tool-item-row', {'tool-item-row-borrowed': !!tool.borrower && !borrowedTool}),
    borrowReturn = !!tool.date ?
      <div className="glyphicon glyphicon-arrow-left" title="Borrow"
           onClick={() => returnTool(tool, toolKey)}></div> :
      <div className="glyphicon glyphicon-wrench" title="Borrow"
           onClick={() => borrowTool(toolKey)}></div>,
    deleteButton = amIHome ? <div className="glyphicon glyphicon-remove" title="Delete"
                                  onClick={() => removeTool(toolKey)}></div> :
      null;
  let borrower = tool.borrower ? tool.borrower : "Available";
  borrower = tool.owner ? 'From - ' + tool.owner : borrower;

  return (
    <div className={rowClass}>
      <div className="row">
        <div className="tool-wrapper">
          <div className="col-xs-4">{tool.name}</div>
          <div className="col-xs-4">
            <div className="glyphicon glyphicon-calendar"></div>
            {borrowedDate}
          </div>
          <div className="col-xs-4">
            <div className="glyphicon glyphicon-user"></div>
            {borrower}
          </div>
        </div>

        {deleteButton}
        {borrowReturn}
      </div>
    </div>
  );
};

Tool.propTypes = {
  tool: React.PropTypes.object.isRequired,
  removeTool: React.PropTypes.func,
  borrowTool: React.PropTypes.func.isRequired,
  returnTool: React.PropTypes.func.isRequired,
  toolClass: React.PropTypes.string,
  username: React.PropTypes.string.isRequired,
  borrowedTool: React.PropTypes.bool
};

export default Tool;
