
import React from 'react';
import autobind from 'autobind-decorator';
import Tool from './Tool.js';
import h from '../helpers';
import Header from './Header/Header';
import randomString from 'random-string';
import toolService from '../services/ToolService';

// Firebase
import Rebase  from 're-base';
let base = Rebase.createClass(h.rebase);
// let gs = new

@autobind
class ToolsInGarage extends React.Component {

  constructor() {
    super();

    this.state = {
      tools : {}
    }
  }

  componentWillReceiveProps(next) {
    if (this.state.garageId != next.routeParams.searchTerm) this.fetchSearchItems(next.routeParams.searchTerm);
  }

  componentWillMount() {
    this.fetchSearchItems(this.props.params.searchTerm);
  }

  fetchSearchItems(searchTerm) {
    base.fetch('toolIndex', {
      context: this,
      asArray: false,
      queries: {
        orderByChild: 'name',
        equalTo: searchTerm
      },
      then(data){
        this.getToolDataFromGarages(data);
      }
    });
  }

  getToolDataFromGarages(toolIndex) {
    this.state.tools = {};
    let keys = Object.keys(toolIndex);

    for (let i=0; i < keys.length; i++) {
      let key = keys[i];
      let tool = toolIndex[key];
      let url = 'garages/' + tool.garage + '/tools/' + tool.toolId;
      toolService.fetchTool(url, (data) => {
        if (data){
          data.url = url;
          data.toolKey = tool.toolId;
          data.owner = tool.garage;
          this.state.tools[key] = data;
        }
        if (keys.length -1 === i) this.setState({tools: this.state.tools});
      });
  };
  }

  newTool(itemKey){
    let toolItem = this.state.tools[itemKey];
    if (toolItem){
    return (
      <Tool key={itemKey} tool={toolItem} toolKey={itemKey} borrowTool={this.borrowTool}
        toolClass={toolItem.name} username='__search__' returnTool={this.returnTool}/>
    )} else {
      return null;
    }
  }

  borrowTool(itemKey) {
    let toolItem = this.state.tools[itemKey],
        currentUser = JSON.parse(h.getUserFromStorage()),
        borrowerKey = randomString({length: 10}),
        url = `garages/${toolItem.owner}/tools/${toolItem.toolKey}`,
        borrowerUrl = `garages/${currentUser.username}/borrowed/${borrowerKey}`;
    toolItem.date = new Date().getTime();
    toolItem.url = null;
    toolItem.borrower = currentUser && currentUser.username ? currentUser.username : 'Anonymous';
    toolItem.borrowerKey = borrowerKey;
    this.state.tools[itemKey] = toolItem;

    base.post(url, {
      data: toolItem,
      then(err){
        if(!err){

        }
      }
    });

    base.post(borrowerUrl, {
      data: toolItem,
      then(err){
        if(!err){

        }
      }
    });

    this.setState({ tools : this.state.tools });
  }

  returnTool(tool, toolKey) {
    let retTool = toolService.returnTool(tool);
    this.state.tools[toolKey] = retTool;

    this.setState({ tools : this.state.tools});
  }

  render() {
    const toolIds = Object.keys(this.state.tools);

    return (
        <div>
        <Header username={JSON.parse(localStorage.getItem('user')).username}/>
          <div className="garage-overlay">
          <div className="row tool-desc-header">
            <div className="tool-wrapper">
              <div className="col-xs-4">Tool Description</div>
              <div className="col-xs-4">Borrowed Date</div>
              <div className="col-xs-4">Borrower</div>
            </div>
          </div>
          <div>{toolIds.map(this.newTool)}</div>
          </div>
        </div>
    )
  }
}


export default ToolsInGarage;
