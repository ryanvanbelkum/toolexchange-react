/*
  Not Found
*/

import React from 'react';

class NotFound extends React.Component {
  render() {
    return (
    <div className="tool-content not-found-content">
      <h1>Please turn around :(</h1>
    </div>
  );
  }
}

export default NotFound;
