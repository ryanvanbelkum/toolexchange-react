
import React from 'react';
import autobind from 'autobind-decorator';
import Tool from './Tool.js'
import h from '../helpers'
import Header from './Header/Header.js'
import Footer from './Footer'
import randomString from 'random-string';
import ToolService from '../services/ToolService';

// Firebase
import Rebase  from 're-base';
const base = Rebase.createClass(h.rebase);

@autobind
class ToolsInGarage extends React.Component {

  constructor() {
    super();

    this.state = {
      tools : {},
      garageId: '',
      firebase: {},
      borrowed : {},
      user: {}
    }
  }

  componentWillReceiveProps(next) {
    if (this.state.garageId != next.routeParams.garageId) this.syncStateWithFirebase(next.routeParams.garageId);
  }

  componentDidMount() {
    let user = JSON.parse(h.getUserFromStorage());
    this.setState({user: user});
    this.syncStateWithFirebase(this.props.params.garageId);
  }

  syncStateWithFirebase(garageId) {
    this.componentWillUnmount();
    const currentUser = JSON.parse(h.getUserFromStorage());
    this.state.firebase.toolRef = base.syncState('garages/' + garageId + '/tools', {
      context : this,
      state : 'tools',
      asArray: false
    });
    this.state.firebase.borrowedRef = base.syncState('garages/' + currentUser.username + '/borrowed', {
      context : this,
      state : 'borrowed',
      asArray: false
    });
  }

  componentWillUnmount() {
    if (this.state.firebase.toolRef) base.removeBinding(this.state.firebase.toolRef);
    if (this.state.firebase.borrowedRef) base.removeBinding(this.state.firebase.borrowedRef);
  }

  addTool(tool){
    let id = randomString({length: 10});
    let index = ToolService.addToolsToIndex(id, tool, this.state.user);
    let toolObj = {
      index: index.key,
      toolKey: id,
      name: tool,
      owner: JSON.parse(h.getUserFromStorage()).username
    };

    this.state.tools[id] = toolObj;
    this.setState({ tools : this.state.tools});
  }

  newTool(itemKey){
    let toolItem = this.state.tools[itemKey];
    if (toolItem){
    return (
      <Tool key={itemKey} tool={toolItem} toolKey={itemKey} borrowTool={this.borrowTool}
        borrowedTool={false} username={this.props.params.garageId} removeTool={this.removeTool} returnTool={this.returnTool}/>
    )} else {
      return null;
    }
  }

  newToolBorrowedByMe(itemKey){
    let toolItem = this.state.borrowed[itemKey];
    if (toolItem){
    return (
      <Tool key={itemKey} tool={toolItem} toolKey={itemKey} borrowTool={this.borrowTool}
        borrowedTool={true} username={this.props.params.garageId} removeTool={this.removeTool} returnTool={this.returnToolBorrowedByMe}/>
    )} else {
      return null;
    }
  }

  borrowTool(itemKey) {
    let toolItem = this.state.tools[itemKey],
        currentUser = this.state.user,
        borrowerKey = randomString({length: 10});
    toolItem.date = new Date().getTime();
    toolItem.borrower = currentUser && currentUser.username ? currentUser.username : 'Anonymous';
    toolItem.borrowerKey = borrowerKey;
    toolItem.owner = this.props.params.garageId;
    this.state.tools[itemKey] = toolItem;
    this.state.borrowed[borrowerKey] = toolItem;

    this.setState({ tools : this.state.tools });
    this.setState({ borrowed : this.state.borrowed });
  }

  returnToolBorrowedByMe(tool, toolKey) {
    const url = 'garages/' + tool.owner + '/tools/' + tool.toolKey;
    tool.date = null;
    tool.borrower = null;
    tool.borrowerKey = null;
    base.post(url, {
      data: tool
    });

    this.state.borrowed[toolKey] = null;
    this.setState({ borrowed : this.state.borrowed });
  }

  returnTool(tool, toolKey) {
    let toolItem = this.state.tools[toolKey];
    this.state.borrowed[toolItem.borrowerKey] = null;
    toolItem.date = null;
    toolItem.borrower = null;
    toolItem.borrowerKey = null;
    this.state.tools[toolKey] = toolItem;

    this.setState({ tools : this.state.tools, borrowed : this.state.borrowed});
  }

  removeTool(deleteKey){
    let tool = this.state.tools[deleteKey];
    this.state.tools[deleteKey] = null;
    this.state.borrowed[tool.borrowerKey] = null;
    ToolService.removeToolsToIndex(tool.index);
    this.setState({ tools : this.state.tools, borrowed : this.state.borrowed});
  }

  render() {
    const toolIds = Object.keys(this.state.tools),
          borrowedToolIds = Object.keys(this.state.borrowed),
          amIHome = h.checkIfHome(this.props.params.garageId),
          addToolBtn = amIHome ? <Footer addToolFunc={this.addTool} />: null,
          borrowedList = amIHome ? <div className="tool-item-row-borrowed-by-me">{borrowedToolIds.map(this.newToolBorrowedByMe)}</div> : null;

    return (
        <div>
        <Header username={this.props.params.garageId}/>
          <div className="garage-overlay">
          <div className="row tool-desc-header">
            <div className="tool-wrapper">
              <div className="col-xs-4">Tool Description</div>
              <div className="col-xs-4">Borrowed Date</div>
              <div className="col-xs-4">Borrower</div>
            </div>
          </div>
          <div>{toolIds.map(this.newTool)}</div>
          {borrowedList}
          </div>
          {addToolBtn}
        </div>
    )
  }
}


export default ToolsInGarage;
