import React from 'react';
import autobind from 'autobind-decorator';
import Header from './Header/Header';
import {ButtonToolbar, Button, Popover, Tooltip, Modal, OverlayTrigger} from 'react-bootstrap';
import h from '../helpers';
import { browserHistory } from 'react-router';

// firebase
import Rebase  from 're-base';
var base = Rebase.createClass(h.rebase);

@autobind
class UserProfile extends React.Component {

  constructor() {
    super();

    this.state = {
      user : {}

    }
  }

  componentWillReceiveProps(next) {
    if (this.state.user.username != next.routeParams.user) this.getStoredUser(next.routeParams.user);
  }

  componentDidMount() {
    this.getStoredUser(this.props.params.user);
  }


  getStoredUser(username) {
    base.fetch(`garages/${username}/profile`, {
      context: this,
      asArray: false,
      then(data){
        this.setState({user : data});
      }
    });
  }

  render() {

    return (
      <div>
        <Header username={JSON.parse(localStorage.getItem('user')).username}/>
        <div className="user-profile">
          <div className='user-profile-header'><span className='glyphicon glyphicon-user glyphicon-user-profile'></span>{this.state.user.firstName} {this.state.user.lastName}</div>
        </div>
      </div>
    )
  }
}

UserProfile.propTypes = {
  username : React.PropTypes.string
};

export default UserProfile;
