import React, { PropTypes } from 'react';
import autobind from 'autobind-decorator';


@autobind
class Footer extends React.Component {
  constructor(props){
    super(props);
    this.toolRef = r => this._tool = r;
  }

  addTool() {
    this.props.addToolFunc(this._tool.value);
  }

  render() {
    return (
      <footer className="footer">
        <form className="tools-form">
          <div className="input-line">
            <input ref={this.toolRef} type="text" className="form-input check-value" placeholder="Hammer, Screw Driver, Phil's Tile Saw"/>
            <button type="button" onClick={this.addTool} className="form-submit btn btn-special">Add Tool!
            </button>
          </div>
        </form>
      </footer>
    );
  }
}

Footer.PropTypes = {
  addToolFunc: PropTypes.func.isRequired,
};

export default Footer;
