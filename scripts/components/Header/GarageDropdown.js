import React from 'react';

const GarageDropdown = ({garages, newGarageItem}) => {
  const garageObjs = Object.keys(garages);

  return (
    <form className="navbar-form navbar-right">
      <div className="dropdown">
        <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Select a Garage
          <span className="caret"></span></button>
        <ul className="dropdown-menu">
          {garageObjs.map(newGarageItem)}
        </ul>
      </div>
    </form>
  );
};

GarageDropdown.propTypes = {
  garages: React.PropTypes.object,
  newGarageItem: React.PropTypes.func
};

export default GarageDropdown;
