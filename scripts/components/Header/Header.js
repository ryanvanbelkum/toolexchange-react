import classNames from 'classnames';
import React from 'react';
import { Link } from 'react-router';
import firebase from 'firebase';
import h from '../../helpers';
import autobind from 'autobind-decorator';
import { browserHistory } from 'react-router';
import GarageDropdown from './GarageDropdown';
import GarageDropdownMobile from './GarageDropdownMobile';
import MyGarageButton from './MyGarageButton';

// Firebase
import Rebase  from 're-base';
const base = Rebase.createClass(h.rebase);

@autobind
class Header extends React.Component {

  constructor() {
    super();

    this.state = {
      availableUsers: {}
    };
  }

  componentDidMount() {
    base.bindToState('userIndex', {
      context: this,
      state: 'availableUsers',
      asArray: false
    });
  }

  getGarageUrl() {
    return JSON.parse(localStorage.getItem('user')).username;
  }

  logout(){
    firebase.auth().signOut().then(function() {
      // Sign-out successful.
    }, function(error) {
      // An error happened.
    });
  }

  search() {
    if (this.refs.searchTerm) {
      browserHistory.push(`/search/${this.refs.searchTerm.value}`);
    }
  }

  newGarageListItem(itemKey){
    const garage = this.state.availableUsers[itemKey],
        url = `/garage/${garage}`;
    return (
      <li key={itemKey}><Link to={url}>{garage}</Link></li>
    );
  }

  render() {
    const garages = this.state.availableUsers ? Object.keys(this.state.availableUsers) : null,
          garageDropdown = garages.length > 0 ? <GarageDropdown garages={this.state.availableUsers} newGarageItem={this.newGarageListItem}/> : null,
          garageDropdownMobile = garages.length > 0 ? <GarageDropdownMobile garages={this.state.availableUsers} newGarageItem={this.newGarageListItem}/> : null,
          buttonClasses = classNames('btn btn-default dropdown-toggle', {
            'disabled': !this.props.username
          }),
          myGarageUrl = '/garage/' + this.getGarageUrl();

    return (
      <nav className="navbar navbar-default" role="navigation">
        <div className="container-fluid">
          <form className="navbar-form navbar-left">
          <Link className="navbar-logo-img" to={"/"}><img className="tool-icon" src="../css/images/garageIcon.png" alt="friendly Garage" /></Link>
            <div className="form-group">
              <input type="text" ref="searchTerm" className="form-control" placeholder="Search" />
            </div>
            <button type="button" className="btn btn-default" onClick={this.search}>Submit</button>
            {garageDropdownMobile}
          </form>
          <div className="collapse navbar-collapse">
            {garageDropdown}
            <MyGarageButton myGarageUrl={myGarageUrl} buttonClasses={buttonClasses} />
          </div>
        </div>
      </nav>

    );
  }
}

Header.propTypes = {
  username : React.PropTypes.string
};

export default Header;
