import React from 'react';
import { Link } from 'react-router';

const GarageDropdown = ({myGarageUrl, buttonClasses}) => {

  return (
    <form className="navbar-form navbar-right">
      <Link to={myGarageUrl}><button className={buttonClasses} type="button">My Garage</button></Link>
    </form>
  );
};

GarageDropdown.propTypes = {
  myGarageUrl: React.PropTypes.string,
  buttonClasses: React.PropTypes.string
};

export default GarageDropdown;
