import React from 'react';

const GarageDropdownMobile = ({garages, newGarageItem}) => {
  const garageObjs = Object.keys(garages);

  return (
    <div className="dropdown-mobile">
      <button type="button" className="navbar-toggle collapsed dropdown-mobile-button" data-toggle="dropdown" aria-expanded="false">
        <span className="sr-only">Toggle navigation</span>
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
      </button>
      <ul className="dropdown-menu">
        {garageObjs.map(newGarageItem)}
      </ul>
    </div>
  );
};

GarageDropdownMobile.propTypes = {
  garages: React.PropTypes.object,
  newGarageItem: React.PropTypes.func
};

export default GarageDropdownMobile;
